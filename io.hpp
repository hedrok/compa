#pragma once

#include <string>

namespace CompA
{

namespace IO
{

int getNumber(const std::string &prompt);
void outputNumber(const std::string &msg, int n);

}

}
