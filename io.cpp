#include <io.hpp>
#include <logger.hpp>

#include <iostream>

namespace CompA
{

namespace IO
{

int getNumber(const std::string &prompt)
{
    CompC::Logger::log("CompA::IO::getNumber");
    std::cout << prompt << ":" << std::endl;
    int r;
    std::cin >> r;
    return r;
}
void outputNumber(const std::string &msg, int n)
{
    CompC::Logger::log("CompA::IO::outputNumber");
    std::cout << msg << " " << n << std::endl;
}

}

}
